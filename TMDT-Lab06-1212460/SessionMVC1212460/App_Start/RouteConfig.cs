﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SessionMVC1212460
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");



            routes.MapRoute(
                name: "Default2",
                url: "{controller}/{sCode}",
                defaults: new { controller = "users", action = "detailByCode" }
            );

            routes.MapRoute(
               name: "Default",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "users", action = "Index", id = UrlParameter.Optional }
           );
           // routes.MapRoute(
           //    name: "Default",
           //    url: "{controller}/{action}",
           //    defaults: new { controller = "Login", action = "Index" }
           //);
            //routes.MapRoute(
            //    "showName", // Route name
            //    "{studentCode}", // URL with parameters
            //    new { action = "DetailsByCode", controller = "users" }, // parameter defaults 
            //    new[] { "SessionMVC1212460.Controllers" } // controller namespaces
            //);
        }
    }
}