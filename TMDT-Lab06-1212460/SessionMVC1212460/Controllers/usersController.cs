﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SessionMVC1212460.Models;

namespace SessionMVC1212460.Controllers
{
    public class usersController : Controller
    {
        //
        // GET: /users/

        public ActionResult Index()
        {
            if(Session["user"] == null)
            {
                Session["URL"] = "users";
                return  Redirect("Login");
            }
            return View();
        }

        public ActionResult detailByCode(int sCode)
        {
            if (Session["user"] == null)
            {
                Session["URL"] =  Server.HtmlEncode(Request.RawUrl);
                //String temp =
                return Redirect("Login");
            }
            marin m = new marin();
            m.getUserByCode(sCode);
            
            return View(m);
        }

    }
}
