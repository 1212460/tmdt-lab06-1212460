﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SessionMVC1212460.Controllers
{
    public class logOutController : Controller
    {
        //
        // GET: /logOut/

        public ActionResult Index()
        {
            Session.Remove("user");
            Session.Remove("pass");
            return Redirect("Login");
        }

    }
}
